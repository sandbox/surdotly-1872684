<?php

/**
 * @file
 * Sur.ly initial settings.
 */
?>
<div class="wrapper-surly">
  <div class="ps-window">
    <div class="ps-top-title">
      <p><?php echo t('Sur.ly plugin settings'); ?></p>
    </div>
    <div class="ps-window-content">
      <div class="ps-left-side">
        <div class="ps-left-content">
          <div class="ps-step-menu">
            <ul id="initial-steps" class="ps-menu">
              <li>
                <div class="ps-num-item"><span>1</span></div>
                <div class="ps-name-item"><p><span><?php echo t('Registration'); ?></span></p></div>
              </li>
              <li>
                <div class="ps-num-item"><span>2</span></div>
                <div class="ps-name-item"><p><span><?php echo t('Domains'); ?></span></p></div>
                <div class="ps-sub-menu">
                  <ul>
                    <li>
                      <span class="sline"></span>
                      <div class="ps-sub-name-item"><p><span><?php echo t('Use your subdomain'); ?></span></p></div>
                    </li>
                    <li>
                      <span class="sline hlast"></span>
                      <div class="ps-sub-name-item"><p><span><?php echo t('Trusted domains'); ?></span></p></div>
                    </li>
                  </ul>
                </div>
              </li>
              <li>
                <div class="ps-num-item"><span>3</span></div>
                <div class="ps-name-item"><p><span><?php echo t('Trusted groups'); ?></span></p></div>
              </li>
              <li>
                <div class="ps-num-item"><span>4</span></div>
                <div class="ps-name-item"><p><span><?php echo t('URL processing'); ?></span></p></div>
                <div class="ps-sub-menu">
                  <ul>
                    <li>
                      <span class="sline"></span>
                      <div class="ps-sub-name-item"><p><span><?php echo t('Shorten URLs'); ?></span></p></div>
                    </li>
                    <li>
                      <div class="ps-sub-name-item"><p><span><?php echo t('Replace URLs'); ?></span></p></div>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="ps-right-side">
        <div class="ps-right-content">
          <div id="surly-initial-step-1-0" data-url="<?php echo url('admin/config/content/surly/ajax/save-toolbar-settings'); ?>" style="display:none">
            <?php if (!variable_get('surly_toolbar_id')): ?>
              <iframe frameborder="0" scrolling="no" class="surly-auth" src="https://surdotly.com/settings/auth/?<?php echo http_build_query(array(
    'url' => url(NULL, array('absolute' => TRUE)),
    'cmsId' => 3,
    'meta' => array('cms_version' => VERSION),
  )
);
?>"></iframe>
            <?php endif; ?>
          </div>
          <form id="surly-initial-step-2-1" data-url="<?php echo url('admin/config/content/surly/ajax/save-subdomain'); ?>" style="display:none">
            <div class="ps-info-text">
              <p><?php echo t('If you have a subdomain set up according to <a href="!url">instructions</a>, just enter its name to allow viewing external pages via it.', array('!url' => 'https://surdotly.com/setting_subdomain#dns')); ?></p>
            </div>
            <div class="ps-cell">
              <div class="surly-field-error" data-field="surly-subdomain"></div>
            </div>
            <div class="ps-next-form">
              <div class="ps-left-row">
                <div class="ps-list">
                  <ul>
                    <li>
                      <div class="ps-type-in">
                        <input
                          name="surly_subdomain"
                          type="text"
                          placeholder="<?php echo t('URL'); ?>"
                          value="<?php echo $surly_subdomain; ?>"/>
                      </div>
                    </li>
                    <li>
                      <div class="ps-type-buttons">
                        <a id="surly-save-subdomain" href="#" class="ps-type-button ps-icon blue"><?php echo t('Next'); ?><span> &rarr;</span></a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </form>
          <div id="surly-initial-step-2-2" style="display:none">
            <div class="ps-info-text">
              <p><?php echo t('List your project’s link building partners (or other trusted websites) here and keep all the outbound linking to their domains & subdomains untouched.'); ?></p>
            </div>
            <div class="ps-next-form">
              <div class="ps-list">
                <ul>
                  <li>
                    <form id="surly-trusted-domains" data-url="<?php echo url('admin/config/content/surly/ajax/save-trusted-domain'); ?>">
                      <div class="ps-adding">
                        <div class="ps-inner-box">
                          <div class="ps-table-box">
                            <div class="ps-cell">
                              <div class="w430">
                                <div class="ps-type-in">
                                  <input id="surly-trusted-domain" name="surly_trusted_domain" value="" placeholder="<?php echo t('Domain'); ?>"/>
                                  <div class="ps-type-buttons">
                                    <a id="surly-save-trusted-domain" href="#" class="ps-type-button blue"><?php echo t('Add domain'); ?></a>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="ps-cell">
                              <div class="surly-field-error" data-field="surly-trusted-domain"></div>
                            </div>
                          </div>
                          <div class="ps-central-box">
                            <div class="ps-table-line">
                              <ul>
                                <li class="first">
                                  <span class="ps-type-check">
                                    <input type="checkbox" id="surly_trusted_domains">
                                  </span>
                                  <label for="surly_trusted_domains"><?php echo t('Domain'); ?></label>
                                  <span class="num-item">
                                    <?php if ($surly_trusted_domains_count > 1): ?>
                                      <?php echo $surly_trusted_domains_count; ?> <?php echo t('items'); ?>
                                    <?php else: ?>
                                      1 <?php echo t('item'); ?>
                                    <?php endif; ?>
                                  </span>
                                </li>
                                <?php foreach (variable_get('surly_trusted_domains', array()) as $key => $value): ?>
                                  <li class="inner">
                                    <span class="ps-type-check">
                                      <input
                                        id="surly_trusted_domains-<?php echo $key; ?>"
                                        name="surly_trusted_domains[]"
                                        value="<?php echo $value; ?>"
                                        type="checkbox"/>
                                    </span>
                                    <label for="surly_trusted_domains-<?php echo $key; ?>"><?php echo $value; ?></label>
                                  </li>
                                <?php endforeach; ?>
                                <?php if (variable_get('surly_trusted_domains', array())): ?>
                                  <li class="empty" style="display:none;"><label><?php echo t('No items found'); ?></label></li>
                                <?php else: ?>
                                  <li class="empty"><label><?php echo t('No items found'); ?></label></li>
                                <?php endif; ?>
                              </ul>
                              <div class="ps-type-buttons">
                                <a id="surly-delete-trusted-domains" data-url="<?php echo url('admin/config/content/surly/ajax/delete-trusted-domains'); ?>" href="#" class="ps-type-button ps-border lred pad30"><?php echo t('Delete'); ?></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </li>
                  <li>
                    <div class="ps-type-buttons">
                      <a id="surly-save-trusted-domains" data-url="<?php echo url('admin/config/content/surly/ajax/skip-step'); ?>" href="#" class="ps-type-button ps-icon blue"><?php echo t('Next'); ?><span> &rarr;</span></a>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <form id="surly-initial-step-3-0" data-url="<?php echo url('admin/config/content/surly/ajax/save-trusted-groups'); ?>" style="display:none">
            <div class="ps-info-text">
              <p><?php echo t('Select the trusted user groups whose links should stay untouched.'); ?></p>
            </div>
            <div class="ps-next-form">
              <div class="ps-left-row">
                <div class="ps-list">
                  <ul>
                    <li>
                      <div class="ps-list-in trusted-groups">
                        <ul>
                          <?php foreach (user_roles() as $key => $value): ?>
                            <li>
                              <label for="surly_trusted_groups-<?php echo $key; ?>">
                                <?php echo ucfirst($value); ?>
                              </label>
                              <input
                                id="surly_trusted_groups-<?php echo $key; ?>"
                                name="surly_trusted_groups[]"
                                value="<?php echo $key; ?>"
                                type="checkbox"
                                <?php if (in_array($key, variable_get('surly_trusted_groups', array()))): ?>
                                  checked="checked"
                                <?php endif; ?>/>
                            </li>
                          <?php endforeach; ?>
                        </ul>
                      </div>
                    </li>
                    <li>
                      <div class="ps-type-buttons">
                        <a id="surly-save-trusted-groups" href="#" class="ps-type-button ps-icon blue"><?php echo t('Next'); ?><span> &rarr;</span></a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </form>
          <form id="surly-initial-step-4-1" data-url="<?php echo url('admin/config/content/surly/ajax/save-shorten-urls'); ?>" style="display:none">
            <div class="ps-info-text">
              <p><?php echo t('Enable URL shortening (optionally): all links replaced by Sur.ly will be shortened and formatted like !url.', array('!url' => 'http://sur.ly/o/bN/' . $surly_toolbar_id)); ?></p>
            </div>
            <div class="ps-next-form">
              <div class="ps-left-row">
                <div class="ps-list">
                  <ul>
                    <li>
                      <div class="ps-select-in">
                        <select name="surly_shorten_urls">
                          <option
                            value="0"
                            <?php if (!variable_get('surly_shorten_urls', FALSE)): ?>
                              selected="selected"
                            <?php endif; ?>>
                              <?php echo t('Disable'); ?>
                          </option>
                          <option
                            value="1"
                            <?php if (variable_get('surly_shorten_urls', FALSE)): ?>
                              selected="selected"
                            <?php endif; ?>>
                              <?php echo t('Enable'); ?>
                          </option>
                        </select>
                      </div>
                    </li>
                    <li>
                      <div class="ps-type-buttons">
                        <a id="surly-save-shorten-urls" href="#" class="ps-type-button ps-icon blue"><?php echo t('Next'); ?><span> &rarr;</span></a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </form>
          <form id="surly-initial-step-4-2" data-url="<?php echo url('admin/config/content/surly/ajax/save-replace-urls'); ?>" style="display:none">
            <div class="ps-info-text">
              <p><?php echo t('Sur.ly can work for links in Comments, Posts, or for all outbound links on your site.'); ?></p>
            </div>
            <div class="ps-next-form">
              <div class="ps-left-row">
                <div class="ps-list">
                  <ul>
                    <li>
                      <div class="ps-list-in">
                        <ul>
                          <li>
                            <label for="surly_replace_urls_nowhere"><?php echo t('Nowhere'); ?></label>
                            <input
                              id="surly_replace_urls_nowhere"
                              name="surly_replace_urls[]"
                              value="0"
                              type="checkbox"
                              <?php if (in_array(0, variable_get('surly_replace_urls', array(0)))): ?>
                                checked="checked"
                              <?php endif; ?>/>
                          </li>
                          <li>
                            <label for="surly_replace_urls_posts"><?php echo t('Posts'); ?></label>
                            <input
                              id="surly_replace_urls_posts"
                              name="surly_replace_urls[]"
                              value="1"
                              type="checkbox"
                              <?php if (in_array(1, variable_get('surly_replace_urls', array(0)))): ?>
                                checked="checked"
                              <?php endif; ?>/>
                          </li>
                          <li>
                            <label for="surly_replace_urls_comments"><?php echo t('Comments'); ?></label>
                            <input
                              id="surly_replace_urls_comments"
                              name="surly_replace_urls[]"
                              value="2"
                              type="checkbox"
                              <?php if (in_array(2, variable_get('surly_replace_urls', array(0)))): ?>
                                checked="checked"
                              <?php endif; ?>/>
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li>
                      <div class="ps-type-buttons">
                        <a id="surly-save-replace-urls" href="#" class="ps-type-button blue"><?php echo t('Finish'); ?></a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $jQuerySurly(document).ready(function() {
    surly.initialStep('<?php echo $surly_initial; ?>');
  });
</script>
