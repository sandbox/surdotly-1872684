<?php

/**
 * @file
 * This class extends Surly SDK class to utilize built-in Drupal cache.
 *
 * @see Surly.php
 */

module_load_include('php', 'surly', 'lib/surly/Surly');

define('SURLY_ACTION_TYPE_INSTALL', 1);
define('SURLY_ACTION_TYPE_AUTH', 2);
define('SURLY_ACTION_TYPE_ACTIVATION', 3);
define('SURLY_ACTION_TYPE_DEACTIVATION', 4);
define('SURLY_ACTION_TYPE_UNINSTALL', 5);
define('SURLY_ACTION_TYPE_SUBDOMAIN_LINK', 6);
define('SURLY_ACTION_TYPE_SUBDOMAIN_UNLINK', 7);

/**
 * Base Surly for Drupal.
 */
class SurlyDrupal extends Surly {

  protected $nid;
  protected $cache;
  protected $rootStatusKey = 'surly_root_status';
  public $isRootDomainAlive = NULL;

  /**
   * Cache short ids.
   */
  public function cacheShortIds($url2short_ids) {
    if (!empty($url2short_ids)) {
      if (!isset($this->cache)) {
        $this->loadCache();
      }
      $this->cache = array_merge($this->cache, $url2short_ids);
      cache_set('surly_node_' . $this->nid, $this->cache);
    }
    parent::cacheShortIds($url2short_ids);
  }

  /**
   * Get cached short ids.
   *
   * @return array
   *   Return cached short ids of urls.
   */
  public function getCachedShortIds($urls) {
    $result = parent::getCachedShortIds($urls);
    if (!empty($urls)) {
      if (!isset($this->cache)) {
        $this->loadCache();
      }
      foreach ($urls as $url) {
        if (!empty($url) && isset($this->cache[$url])) {
          $result[$url] = $this->cache[$url];
        }
      }
    }
    return $result;
  }

  /**
   * Set nid.
   *
   * @param string $nid
   *   Node id.
   */
  public function setNid($nid) {
    if ($this->nid !== $nid) {
      $this->nid = $nid;
      unset($this->cache);
    }
  }

  /**
   * Load cache.
   */
  public function loadCache() {
    $cache = cache_get('surly_node_' . $this->nid);
    if ($cache) {
      $this->cache = $cache->data;
    }
    else {
      $this->cache = array();
    }
  }

  /**
   * Get status from cache.
   *
   * @return string|false
   *   Return status or false if not found.
   */
  public function getCachedRootStatus() {
    $cache_object = cache_get($this->rootStatusKey);

    if ($cache_object) {
      return $cache_object->data;
    }

    return FALSE;
  }

  /**
   * Add status to cache.
   *
   * @param string $root_status
   *   Status.
   */
  public function cacheRootStatus($root_status) {
    cache_set($this->rootStatusKey, $root_status);
  }

  /**
   * Tracking.
   */
  public function trackHistory($action_type) {
    $this->timeout = SURLY_API_TRACK_TIMEOUT;

    $this->_performRequest(
      $this->apiHost . SURLY_API_TRACK_HISTORY_PATH, 'POST',
      array(
        'action_type' => $action_type,
        'site_url' => url(NULL, array('absolute' => TRUE)),
        'toolbar_id' => variable_get('surly_toolbar_id', NULL),
        'hash' => variable_get('surly_activation_hash', NULL),
      )
    );

    $this->timeout = SURLY_API_TIMEOUT;
  }

  /**
   * Link subdomain.
   */
  public function linkSubdomain($subdomain) {
    return $this->_performRequest(
      $this->apiHost . SURLY_API_SUBDOMAIN_LINK, 'POST',
      array(
        'toolbar_id' => variable_get('surly_toolbar_id'),
        'password' => variable_get('surly_toolbar_password'),
        'subdomain' => $subdomain,
      )
    );
  }

}
