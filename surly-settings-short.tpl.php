<?php

/**
 * @file
 * Sur.ly short settings.
 */
?>
<div class="wrapper-surly">
  <div id="surly-message" class="messages status" style="display:none">
    <h2 class="element-invisible">Status message</h2>
    <?php echo t('Settings saved.'); ?>
  </div>
  <div class="ps-window">
    <div class="ps-central-content">
      <div class="ps-top-title">
        <p>Sur.ly</p>
      </div>
      <div class="ps-rows-settings">
        <form id="surly-save-settings-form" data-url="<?php echo url('admin/config/content/surly/ajax/save-settings'); ?>">
          <div id="surly-replace-urls" class="ps-row-box">
            <div class="ps-inner-box">
              <div class="ps-title-box">
                <p><?php echo t('Replace URLs'); ?></p>
              </div>
              <div class="ps-table-box">
                <div class="ps-cell">
                  <div class="w280">
                    <div
                      <?php if (in_array(0, variable_get('surly_replace_urls', array(0)))): ?>
                        class="ps-list-in field-error"
                      <?php else: ?>
                        class="ps-list-in"
                      <?php endif; ?>>
                      <ul>
                        <li>
                          <label for="surly_replace_urls_nowhere"><?php echo t('Nowhere'); ?></label>
                          <input
                            id="surly_replace_urls_nowhere"
                            name="surly_replace_urls[]"
                            value="0"
                            type="checkbox"
                            <?php if (in_array(0, variable_get('surly_replace_urls', array(0)))): ?>
                              checked="checked"
                            <?php endif; ?>/>
                        </li>
                        <li>
                          <label for="surly_replace_urls_posts"><?php echo t('Posts'); ?></label>
                          <input
                            id="surly_replace_urls_posts"
                            name="surly_replace_urls[]"
                            value="1"
                            type="checkbox"
                            <?php if (in_array(1, variable_get('surly_replace_urls', array(0)))): ?>
                              checked="checked"
                            <?php endif; ?>/>
                        </li>
                        <li>
                          <label for="surly_replace_urls_comments"><?php echo t('Comments'); ?></label>
                          <input
                            id="surly_replace_urls_comments"
                            name="surly_replace_urls[]"
                            value="2"
                            type="checkbox"
                            <?php if (in_array(2, variable_get('surly_replace_urls', array(0)))): ?>
                              checked="checked"
                            <?php endif; ?>/>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="ps-cell">
                  <div class="surly-field-error" data-field="surly-replace-urls">
                    <div
                      <?php if (in_array(0, variable_get('surly_replace_urls', array(0)))): ?>
                        class="ps-title-cell red" style="display:block"
                      <?php else: ?>
                        style="display:none;"
                      <?php endif; ?>>
                      <p><?php echo t('Your website and visitors are not yet protected by Sur.ly. Be sure to enable it for your outbound links.'); ?></p>
                    </div>
                  </div>
                  <div class="ps-title-cell">
                    <p><?php echo t('Sur.ly can work for links in Comments, Posts, or for all outbound links on your site.'); ?></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="ps-type-buttons">
        <a id="surly-save-settings" href="#" class="ps-type-button blue"><?php echo t('Save changes'); ?></a>
      </div>
      <div class="ps-rows-settings">
        <div class="ps-row-box">
          <div class="ps-inner-box">
            <div class="ps-title-box">
              <p><?php echo t('Customize Sur.ly for maximum effect'); ?></p>
            </div>
            <div class="ps-show-info">
              <a id="surly-set-up-plugin-img" data-url="<?php echo url('admin/config/content/surly/ajax/skip-step'); ?>" href="#"><div class="toolbar-img"></div></a>
            </div>
          </div>
          <div class="ps-type-buttons ps-mess">
            <a id="surly-set-up-plugin" data-url="<?php echo url('admin/config/content/surly/ajax/skip-step'); ?>" href="#" class="ps-type-button blue"><?php echo t('Configure Sur.ly'); ?></a>
            <p class="ps-start"><?php echo t('and get the most out of outbound links with fine tuning.'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
