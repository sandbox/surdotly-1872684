<?php

/**
 * @file
 * Sur.ly toolbar settings.
 */
?>
<div class="wrapper-surly">
  <div class="ps-window">
    <div class="ps-central-content">
      <div class="ps-rows-settings">
        <div class="ps-row-box">
          <div class="ps-inner-box">
            <div class="ps-title-box">
              <p><?php echo t('Configure your toolbar'); ?></p>
            </div>
            <div class="ps-settings-out">
              <iframe frameborder="0" scrolling="no" class="surly-settings-toolbar" name="settings" src="<?php echo SURLY_PANEL_URL . $surly_toolbar_id . '/plugin_access?password=' . $surly_toolbar_password; ?>"></iframe>
            </div>
          </div>
        </div>
      </div>
      </div>
  </div>
</div>
