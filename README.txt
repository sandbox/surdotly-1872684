
CONTENTS OF THIS FILE
---------------------

 * Description
 * Installation


DESCRIPTION
------------

Homepage: http://sur.ly

Sur.ly module protects your outbound links in comments and post (configurable) 
by replacing them with safe links. Once a visitor clicks on a replaced link, 
he will be directed to a special interstitial page on Sur.ly (http://sur.ly).
Sur.ly analyzes the target page and warns the visitor about any potential 
threats or adult content.

Links published by users can be too long or contain some bad words. Optionally 
Sur.ly module allows to shorten links by replacing them with alphanumeric 
identifiers that look neat and convenient.

Please note that additional request to Sur.ly will be sent when link 
shortening is enabled.

Feel free to contact us at contact@surdotly.com


INSTALLATION
------------

1. Copy this surly/ directory to your sites/SITENAME/modules directory.

2. Download Surly library (http://cdn.sur.ly/plugins/surly-sdk-php.zip)
   and extract its contents to the following subdirectory 'lib' of this module.

3. Enable the module.

4. Configure replacement options admin/config/content/surly.
