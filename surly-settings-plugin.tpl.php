<?php

/**
 * @file
 * Sur.ly plugin settings.
 */
?>
<div class="wrapper-surly">
  <div id="surly-message" class="messages status" style="display:none">
    <?php echo t('Settings saved.'); ?>
  </div>
  <div class="ps-window">
    <div class="ps-central-content">
      <div class="ps-rows-settings">
        <form id="surly-save-settings-form" data-url="<?php echo url('admin/config/content/surly/ajax/save-settings'); ?>">
          <div id="surly-replace-urls" class="ps-row-box">
            <div class="ps-inner-box">
              <div class="ps-title-box">
                <p><?php echo t('Replace URLs'); ?></p>
              </div>
              <div class="ps-table-box">
                <div class="ps-cell">
                  <div class="w280">
                    <div
                      <?php if (in_array(0, variable_get('surly_replace_urls', array(0)))): ?>
                        class="ps-list-in field-error"
                      <?php else: ?>
                        class="ps-list-in"
                      <?php endif; ?>>
                      <ul>
                        <li>
                          <label for="surly_replace_urls_nowhere"><?php echo t('Nowhere'); ?></label>
                          <input
                            id="surly_replace_urls_nowhere"
                            name="surly_replace_urls[]"
                            value="0"
                            type="checkbox"
                            <?php if (in_array(0, variable_get('surly_replace_urls', array(0)))): ?>
                              checked="checked"
                            <?php endif; ?>/>
                        </li>
                        <li>
                          <label for="surly_replace_urls_posts"><?php echo t('Posts'); ?></label>
                          <input
                            id="surly_replace_urls_posts"
                            name="surly_replace_urls[]"
                            value="1"
                            type="checkbox"
                            <?php if (in_array(1, variable_get('surly_replace_urls', array(0)))): ?>
                              checked="checked"
                            <?php endif; ?>/>
                        </li>
                        <li>
                          <label for="surly_replace_urls_comments"><?php echo t('Comments'); ?></label>
                          <input
                            id="surly_replace_urls_comments"
                            name="surly_replace_urls[]"
                            value="2"
                            type="checkbox"
                            <?php if (in_array(2, variable_get('surly_replace_urls', array(0)))): ?>
                              checked="checked"
                            <?php endif; ?>/>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="ps-cell">
                  <div class="surly-field-error" data-field="surly-replace-urls">
                    <div
                      <?php if (in_array(0, variable_get('surly_replace_urls', array(0)))): ?>
                        class="ps-title-cell red" style="display:block"
                      <?php else: ?>
                        style="display:none;"
                      <?php endif; ?>>
                      <p><?php echo t('Your website and visitors are not yet protected by Sur.ly. Be sure to enable it for your outbound links.'); ?></p>
                    </div>
                  </div>
                  <div class="ps-title-cell">
                    <p><?php echo t('Sur.ly can work for links in Comments, Posts, or for all outbound links on your site.'); ?></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="ps-row-box">
            <div class="ps-inner-box">
              <div class="ps-title-box">
                <p><?php echo t('Shorten URLs'); ?></p>
              </div>
              <div class="ps-cell">
                <div class="w280">
                  <div class="ps-cell">
                    <div class="ps-select-in">
                      <select name="surly_shorten_urls">
                        <option
                          value="0"
                          <?php if (!variable_get('surly_shorten_urls', FALSE)): ?>
                            selected="selected"
                          <?php endif; ?>>
                            <?php echo t('Disable'); ?>
                        </option>
                        <option
                          value="1"
                          <?php if (variable_get('surly_shorten_urls', FALSE)): ?>
                            selected="selected"
                          <?php endif; ?>>
                            <?php echo t('Enable'); ?>
                        </option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="ps-cell">
                <div class="ps-title-cell">
                  <p><?php echo t('Enable URL shortening (optionally): all links replaced by Sur.ly will be shortened and formatted like !url.', array('!url' => 'http://sur.ly/o/bN/' . $surly_toolbar_id)); ?></p>
                </div>
              </div>
            </div>
          </div>
          <div id="surly-subdomain" class="ps-row-box">
            <div class="ps-inner-box">
              <div class="ps-title-box">
                <p><?php echo t('Use your subdomain'); ?></p>
              </div>
              <div class="ps-table-box">
                <div class="ps-cell">
                  <div class="w280">
                    <div class="ps-type-in">
                      <input
                        name="surly_subdomain"
                        type="text"
                        placeholder="<?php echo t('URL'); ?>"
                        value="<?php echo $surly_subdomain; ?>"/>
                    </div>
                  </div>
                </div>
                <div class="ps-cell">
                  <div class="surly-field-error" data-field="surly-subdomain"></div>
                  <div class="ps-title-cell">
                    <p><?php echo t('If you have a subdomain set up according to <a href="!url">instructions</a>, just enter its name to allow viewing external pages via it.', array('!url' => 'https://surdotly.com/setting_subdomain#dns')); ?></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="ps-row-box">
            <div class="ps-inner-box">
              <div class="ps-title-box">
                <p><?php echo t('Trusted groups'); ?></p>
              </div>
              <div class="ps-table-box">
                <div class="ps-cell">
                  <div class="w280">
                    <div class="ps-list-in trusted-groups">
                      <ul>
                        <?php foreach (user_roles() as $key => $value): ?>
                          <li>
                            <label for="surly_trusted_groups-<?php echo $key; ?>">
                              <?php echo ucfirst($value); ?>
                            </label>
                            <input
                              id="surly_trusted_groups-<?php echo $key; ?>"
                              name="surly_trusted_groups[]"
                              value="<?php echo $key; ?>"
                              type="checkbox"
                              <?php if (in_array($key, variable_get('surly_trusted_groups', array()))): ?>
                                checked="checked"
                              <?php endif; ?>/>
                          </li>
                        <?php endforeach; ?>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="ps-cell">
                  <div class="ps-title-cell">
                    <p><?php echo t('Select the trusted user groups whose links should stay untouched.'); ?></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
        <form id="surly-trusted-domains" data-url="<?php echo url('admin/config/content/surly/ajax/save-trusted-domain'); ?>">
          <div class="ps-row-box">
            <div class="ps-inner-box">
              <div class="ps-title-box">
                <p><?php echo t('Trusted domains'); ?></p>
              </div>
              <div class="ps-table-box">
                <div class="ps-cell">
                  <div class="w430">
                    <div class="ps-type-in">
                      <input id="surly-trusted-domain" name="surly_trusted_domain" value="" placeholder="<?php echo t('Domain'); ?>"/>
                      <div class="ps-type-buttons">
                        <a id="surly-save-trusted-domain" href="#" class="ps-type-button blue"><?php echo t('Add domain'); ?></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="ps-cell">
                  <div class="surly-field-error" data-field="surly-trusted-domain"></div>
                  <div class="ps-title-cell">
                    <p><?php echo t('List your project’s link building partners (or other trusted websites) here and keep all the outbound linking to their domains & subdomains untouched.'); ?></p>
                  </div>
                </div>
              </div>
              <div class="ps-central-box">
                <div class="ps-table-line">
                  <ul>
                    <li class="first">
                      <span class="ps-type-check">
                        <input type="checkbox" value="1" id="surly_trusted_domains"/>
                      </span>
                      <label for="surly_trusted_domains"><?php echo t('Domain'); ?></label>
                      <span class="num-item">
                        <?php if ($surly_trusted_domains_count > 1): ?>
                          <?php echo $surly_trusted_domains_count; ?> <?php echo t('items'); ?>
                        <?php else: ?>
                          1 <?php echo t('item'); ?>
                        <?php endif; ?>
                      </span>
                    </li>
                    <?php foreach (variable_get('surly_trusted_domains', array()) as $key => $value): ?>
                      <li class="inner">
                        <span class="ps-type-check">
                          <input
                            id="surly_trusted_domains-<?php echo $key; ?>"
                            name="surly_trusted_domains[]"
                            value="<?php echo $value; ?>"
                            type="checkbox"/>
                        </span>
                        <label for="surly_trusted_domains-<?php echo $key; ?>"><?php echo $value; ?></label>
                      </li>
                    <?php endforeach; ?>
                    <?php if (variable_get('surly_trusted_domains', array())): ?>
                      <li class="empty" style="display:none;"><label><?php echo t('No items found'); ?></label></li>
                    <?php else: ?>
                      <li class="empty"><label><?php echo t('No items found'); ?></label></li>
                    <?php endif; ?>
                  </ul>
                  <div class="ps-type-buttons">
                    <a id="surly-delete-trusted-domains" data-url="<?php echo url('admin/config/content/surly/ajax/delete-trusted-domains'); ?>" href="#" class="ps-type-button ps-border lred pad30"><?php echo t('Delete'); ?></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="ps-type-buttons">
        <a id="surly-save-settings" href="#" class="ps-type-button blue"><?php echo t('Save changes'); ?></a>
      </div>
    </div>
  </div>
</div>
